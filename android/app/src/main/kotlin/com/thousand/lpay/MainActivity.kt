package com.thousand.lpay

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.share.Sharer
import com.facebook.share.model.*
import com.facebook.share.widget.ShareDialog
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity : FlutterActivity() {

    private val CHANNEL = "com.thousand.lpay"

    var callbackManager : CallbackManager? = null

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler{ call, result ->
            printFacebookHashKey(context)
            if (call.method == "openFacebookApp"){
                if (!isFacebookInstalled()){
                    Toast.makeText(
                        context,
                        "Требуется установить приложение Facebook",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setMethodCallHandler
                }

                getBitmapFromUri(call.arguments.toString())
            }
        }
    }

    private fun getBitmapFromUri(uri: String){
        Glide.with(context)
            .asBitmap()
            .load(uri)
            .addListener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.let {
                        runOnUiThread {
                            openFacebookApp(it)
                        }
                    }
                    return true
                }
            }).submit()
    }

    private fun openFacebookApp(bitmap: Bitmap){
        val sharePhoto = SharePhoto.Builder()
            .setBitmap(bitmap)
            .build()

        val sharePhotoContent = SharePhotoContent.Builder()
            .addPhoto(sharePhoto)
            .build()


        val shareDialog = ShareDialog(this)
        shareDialog.show(sharePhotoContent)
        callbackManager = CallbackManager.Factory.create()
        shareDialog.registerCallback(callbackManager, object : FacebookCallback<Sharer.Result> {
            override fun onSuccess(result: Sharer.Result?) {
                Log.i("FACEBOOK", "onSuccess")
            }

            override fun onCancel() {
                Log.i("FACEBOOK", "onCancel")
            }

            override fun onError(error: FacebookException?) {
                Log.i("FACEBOOK", "onError")
                error?.printStackTrace()
            }
        })
    }

    private fun isFacebookInstalled(): Boolean = try {
            packageManager?.getPackageInfo("com.facebook.katana", 0)
            true
        }catch (e: PackageManager.NameNotFoundException){ false }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    private fun printFacebookHashKey(pContext: Context) {
        val TAG = "LPPPPAAAYY"
        try {
            val info: PackageInfo = pContext.getPackageManager()
                .getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.i(
                    TAG,
                    "printHashKey() Hash Key: $hashKey"
                )
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "printHashKey()", e)
        } catch (e: Exception) {
            Log.e(TAG, "printHashKey()", e)
        }
    }
}