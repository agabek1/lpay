import 'dart:io';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_social_content_share/flutter_social_content_share.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/user.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:provider/provider.dart';
import 'package:social_share_plugin/social_share_plugin.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';

class AddPostSocial extends StatefulWidget {
  final File file;
  final String imageUrl;
  final String des;

  const AddPostSocial({this.file, this.imageUrl, this.des});
  @override
  _AddPostSocialState createState() => _AddPostSocialState();
}

class _AddPostSocialState extends State<AddPostSocial> {
  @override
  bool _vkChange = false;
  bool _instagramChange = false;
  bool _facebookChange = false;
  bool _twitterChange = false;
  File file;
  String msgCopy = 'Текст для поста скопирован в буфер обмена. Вы можете вставить отредактировать его на следующем экране.';

  Widget build(BuildContext context) {
    final User user = Provider.of<UserProvider>(context, listen: false).user;
    PostProvider postProvider = Provider.of<PostProvider>(context);

    postProvider.vkChange = _vkChange;
    postProvider.instagramChange = _instagramChange;
    postProvider.facebookChange = _facebookChange;
    postProvider.twitterChange = _twitterChange;

    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Опубликовать в",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          ListTile(
            onTap: (){
               if (user.vkAccessToken != null){
                 postProvider.vkAdd();
                 ScaffoldMessenger.of(context).showSnackBar(
                   SnackBar(
                     content: Text('опубликовано вконтакте'),
                   ),
                 );
               }
            },
            leading: SvgPicture.asset(
              'images/icons/vk.svg',
              width: 15,
              height: 15,
              color: user.vkAccessToken.length > 5 ? purple : Colors.blueGrey,
            ),
            title: Text('VKontakte',style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),

            // trailing: CupertinoSwitch(
            //   value: _vkChange,
            //   onChanged: (value) {
            //     if (user.vkAccessToken.length > 5) {
            //       setState(() {
            //         _vkChange = !_vkChange;
            //       });
            //     }
            //   },
            // ),
          ),
          //instagram
          ListTile(
            leading: SvgPicture.asset(
              'images/icons/instagram.svg',
              width: 20,
              height: 20,
              color: user.instagram != "null" ? purple : Colors.blueGrey,
            ),
            title: Text(
              'Instagram',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () async {
              if (widget.file != null) {
                FlutterClipboard.copy(widget.des);
                await alertMsg(msgCopy);
                FlutterSocialContentShare.share(
                  type: ShareType.instagramWithImageUrl,
                  imageUrl: widget.imageUrl,
                  quote: widget.des,
                );
              }
            },
          ),
          //facebook
          ListTile(
            onTap: () async {
              if (widget.file != null) {
                FlutterClipboard.copy(widget.des);
                await alertMsg(msgCopy);
                // if (Platform.isIOS) {
                //   SocialSharePlugin.shareToFeedFacebook(
                //     caption: widget.des,
                //     path: widget.file.path,
                //   );
                // } else if (Platform.isAndroid) {
                //   FlutterSocialContentShare.share(
                //     type: ShareType.facebookWithoutImage,
                //     url: 'widget.imageUrl',
                //     quote: widget.des,
                //   );
                // }
                if (Platform.isIOS) {
                  SocialSharePlugin.shareToFeedFacebook(
                    caption: widget.des,
                    path: widget.file.path,
                  );
                } else if (Platform.isAndroid) {
                  const platform = MethodChannel('com.thousand.lpay');
                  await platform.invokeMethod("openFacebookApp", widget.file.path);
                }
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Пожалуйста, выберите фотографию'),
                  ),
                );
              }
            },
            leading: SvgPicture.asset(
              'images/icons/facebook.svg',
              width: 20,
              height: 20,
              color:
                  user.facebookAccessToken != "null" ? purple : Colors.blueGrey,
            ),
            title: Text(
              "Facebook",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          ListTile(
            onTap: () async {
              FlutterClipboard.copy(widget.imageUrl);
              await alertMsg(msgCopy);
              await SocialSharePlugin.shareToTwitterLink(
                text: widget.des.replaceAll('#', ''),
                url: widget.imageUrl,
              );
            },
            leading: SvgPicture.asset(
              'images/icons/twitter.svg',
              width: 20,
              height: 20,
              color: user.twitter != "null" ? purple : Colors.blueGrey,
            ),
            title: Text(
              'Twitter',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
            // trailing: CupertinoSwitch(
            //   value: _twitterChange,
            //   onChanged: (value) {
            //     if (user.twitter != "null") {
            //       setState(() {
            //         _twitterChange = !_twitterChange;
            //       });
            //     }
            //   },
            // ),
          ),
          const Text(
            'Получите больше лайков, опубликовав пост в каждом из социальных сетей(Лайки суммируются автоматический)',
          )
        ],
      ),
    );
  }

  Future<bool> alertMsg(String txt) async {
    await showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            content: Container(
              child: Center(
                child: Text(
                  txt,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            actions: [
              CupertinoButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
    return true;
  }
}

enum Share { facebook, twitter, whatsapp, whatsapp_business, share_system }
