import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:provider/provider.dart';


class AddPostMainInfo extends StatefulWidget {
  @override
  _AddPostMainInfoState createState() => _AddPostMainInfoState();
}

class _AddPostMainInfoState extends State<AddPostMainInfo> {
  @override
  Widget build(BuildContext context) {
    Establishment establishment = Provider.of<PostProvider>(context,listen: false).establishment;
    return Container(
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.all(16),
      color: Colors.white,
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.only(right: 16),
            child: ClipOval(
              child:
              Container(child: SvgPicture.asset('images/default_logo.svg')),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                establishment.cat.name,
                style: TextStyle(fontSize: 14),
              ),
              Text(
                establishment.name,
                style: TextStyle(fontSize: 16),
              ),
            ],
          )
        ],
      ),
    );
  }
}

