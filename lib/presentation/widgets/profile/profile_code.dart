import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/user.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:provider/provider.dart';

class ProfileCode extends StatefulWidget {
  @override
  _ProfileCodeState createState() => _ProfileCodeState();
}

class _ProfileCodeState extends State<ProfileCode> {
  User user;
  @override
  Widget build(BuildContext context) {
    user = context.watch<UserProvider>().user;
    return Container(
      height: 128,
      width: 260,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300].withOpacity(0.7),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Stack(
        children: [
          SvgPicture.asset('images/login_code_bg.svg'),
          Positioned(
            bottom: 50,
            left: 95,
            child: Text(
              user.id.toString(),
              style: TextStyle(
                fontSize: 28,
                color: purple,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          const Positioned(
            bottom: 30,
            left: 5,
            child: Text('Код клиента. Покажите его при оплате'),
          )
        ],
      ),
    );
  }
}
