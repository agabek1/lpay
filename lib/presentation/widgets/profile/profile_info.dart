
// import 'package:android_intent/android_intent.dart';
// import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_login_vk/flutter_login_vk.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:provider/provider.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:image_picker/image_picker.dart';

// import 'package:intent/intent.dart' as android_intent;
// import 'package:intent/extra.dart' as android_extra;
// import 'package:intent/typedExtra.dart' as android_typedExtra;
// import 'package:intent/action.dart' as android_action;
class ProfileInfo extends StatefulWidget {
  ProfileInfo({Key key}) : super(key: key);

  @override
  _ProfileInfoState createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);

    return Container(
      height: 180,
      margin: EdgeInsets.only(top: 14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: ClipOval(
              child: Material(
                color: purple, // button color
                child: InkWell(
                  splashColor: Colors.green, // inkwell color
                  child: SizedBox(
                    width: 90,
                    height: 90,
                    child: Icon(
                      Icons.perm_identity,
                      color: Colors.white,
                      size: 45,
                    ),
                  ),
                  onTap: () {},
                ),
              ),
            ),
          ),
          Text(
            userProvider.user.name,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              VkWidget(),
              SvgPicture.asset(
                'images/icons/twitter.svg',
                color: Color.fromRGBO(148, 162, 171, 0.69),
              ),
              SvgPicture.asset(
                'images/icons/instagram.svg',
                color: Color.fromRGBO(148, 162, 171, 0.69),
              ),
              FacebookWidget(),
            ],
          ),
          Text('Привяжите аккаунты соц. сетей'),
        ],
      ),
    );
  }
}

class VkWidget extends StatefulWidget {
  final plugin = VKLogin(debug: false);

  VkWidget({Key key}) : super(key: key);

  @override
  _VkWidgetState createState() => _VkWidgetState();
}

class _VkWidgetState extends State<VkWidget> {
  String _sdkVersion = vkSdkVersion;
  VKAccessToken _token;
  VKUserProfile _profile;
  String _email;
  bool _sdkInitialized = false;

  @override
  void initState() {
    _initSdk();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final token = _token;
    final profile = _profile;
    final isLogin = token != null;

    final userProvider = Provider.of<UserProvider>(context);

    return   GestureDetector(
      onTap: () {
        userProvider.user.vkAccessToken != "null"
            ? _onPressedLogOutButton()
            : _onPressedLogInButton(context);
      },
      child: SvgPicture.asset(
        'images/icons/vk.svg',
        color:  userProvider.user.vkAccessToken != "null" ? purple : Color.fromRGBO(148, 162, 171, 0.69),
      ),
    );
  }

  Future<void> _onPressedLogInButton(BuildContext context) async {
    final res = await widget.plugin.logIn(scope: [
      VKScope.wall,
      VKScope.photos,
    ]);

    if (res.isError) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Log In failed: ${res.asError.error}'),
        ),
      );
    } else {
      final loginResult = res.asValue.value;
      if (!loginResult.isCanceled) await _updateLoginInfo();

      if (_token != null) {
        if (_token.token.length > 6) {

          Provider.of<UserProvider>(context, listen: false)
              .edit({"vk_access_token": _token.token,"vk_id":_profile.userId});

          Provider.of<UserProvider>(context, listen: false).user.vkAccessToken = _token.token;
          Provider.of<UserProvider>(context, listen: false).user.vkId = _profile.userId.toString();
        }
      }
    }
  }

  Future<void> _onPressedLogOutButton() async {
    print('click log out');
    await widget.plugin.logOut();
    await _updateLoginInfo();
    Provider.of<UserProvider>(context, listen: false).edit({"vk_access_token": "null",'vk_id':"null"});
    Provider.of<UserProvider>(context, listen: false).user.vkAccessToken = "null";
    Provider.of<UserProvider>(context, listen: false).user.vkId = "null";
  }

  Future<void> _initSdk() async {
    await widget.plugin.initSdk(vkAppId);
    _sdkInitialized = true;
    await _updateLoginInfo();
  }

  Future<void> _getSdkVersion() async {
    final sdkVersion = await widget.plugin.sdkVersion;
    setState(() {
      _sdkVersion = sdkVersion;
    });
  }

  Future<void> _updateLoginInfo() async {
    if (!_sdkInitialized) return;

    final plugin = widget.plugin;
    final token = await plugin.accessToken;
    final profileRes = token != null ? await plugin.getUserProfile() : null;
    final email = token != null ? await plugin.getUserEmail() : null;

    setState(() {
      _token = token;
      _profile = profileRes?.asValue?.value;
      _email = email;
    });
  }
}


class FacebookWidget extends StatefulWidget {
  // const FacebookWidget({Key? key}) : super(key: key);

  @override
  _FacebookWidgetState createState() => _FacebookWidgetState();
}

class _FacebookWidgetState extends State<FacebookWidget> {
  //
  // Future<String> _getPath() async {
  //   return ExtStorage.getExternalStorageDirectory();   // /storage/emulated/0/Download
  // }
  // static const platform = MethodChannel('app.channel.shared.data');
  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);


    return  GestureDetector(
      onTap: () async {




        // if (Platform.isAndroid) {
        //   AndroidIntent intent = AndroidIntent(
        //     action: 'action_view',
        //     type: 'image/*',
        //     data: Uri.en,
        //     package: 'com.instagram.android'
        //   );
        //   await intent.launch();
        // }

        // android_intent.Intent()
        //   ..setAction(android_action.Action.ACTION_SEND)
        //   ..setType('image/*')
        //   ..putExtra(android_extra.Extra.EXTRA_PACKAGE_NAME,'com.instagram.android')
        //   ..setData(uri)
        //   ..startActivity().catchError((e) => print(e));

        if  (userProvider.user.facebookAccessToken != 'null'){
          await FacebookAuth.instance.logOut();

          Provider.of<UserProvider>(context, listen: false).edit({"facebook_access_token": "null",'facebook_user_id':""});
          Provider.of<UserProvider>(context, listen: false).user.facebookUserId = "null";
          Provider.of<UserProvider>(context, listen: false).user.facebookAccessToken = "null";

        }else{

          final LoginResult result = await FacebookAuth.instance.login();
          if (result.status == LoginStatus.success) {
            final AccessToken accessToken = result.accessToken;
            Provider.of<UserProvider>(context, listen: false).edit({"facebook_access_token": accessToken.token,"facebook_user_id":accessToken.userId});
          }
        }
      },
      child: SvgPicture.asset(
        'images/icons/facebook.svg',
        color: userProvider.user.facebookAccessToken != "null" ? purple:Color.fromRGBO(148, 162, 171, 0.69),
      ),
    );
  }
}

