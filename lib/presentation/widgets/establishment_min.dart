import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/presentation/pages/establishment_page.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:provider/provider.dart';

class EstablishmentMin extends StatefulWidget {
  final Establishment establishment;
  EstablishmentMin({@required this.establishment});


  @override
  _EstablishmentMinState createState() => _EstablishmentMinState();
}

class _EstablishmentMinState extends State<EstablishmentMin> {

  @override
  Widget build(BuildContext context) {


    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return EstablishmentPage(id: widget.establishment.id,);
          }),
        );
      },
      child: Container(
          margin: EdgeInsets.only(top: 12),
          height: 160,
          decoration: BoxDecoration(
            image: DecorationImage(
              image:  NetworkImage(serverUrl + widget.establishment.images[0]),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(8)
          ),
          child: Stack(
            children: [
              Positioned(
                  top: 12,
                  left: 12,
                  child: Row(
                    children: [
                      Container(
                        child: SvgPicture.asset(
                          'images/default_logo.svg',
                          color: Colors.red,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.establishment.cat.name,
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              widget.establishment.name,
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
              Positioned(
                bottom: 0,
                left: 0,
                child: Container(
                  width: 100,
                  height: 36,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                    ),
                  ),
                  child: Row(
                    children: [
                      Text('Скидка до:',style: TextStyle(fontSize: 12),),
                      Text('${widget.establishment.discount}%',style: TextStyle(fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                child: Container(
                  margin: EdgeInsets.only(right: 12,bottom: 10),
                  child: Text("${widget.establishment.price}т = ${widget.establishment.like} лайк ",style: TextStyle(color: Colors.white,fontSize: 16)),
                ),
              )
            ],
          )),
    );
  }
}
