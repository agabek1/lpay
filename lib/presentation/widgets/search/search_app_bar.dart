import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/providers/cat_provider.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:provider/provider.dart';

class SearchAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  _State createState() => _State();
}

class _State extends State<SearchAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      backgroundColor: Colors.white,
      title: Text(
        "Список заведении",
        style: TextStyle(color: Colors.black),
      ),
      elevation: 0.0,
      automaticallyImplyLeading: false,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.filter_alt,
            color: purple,
          ),
          onPressed: () {
            showModalBottomSheet<void>(
              context: context,
              builder: (BuildContext context) {
                return BottomSheet();
              },
            );
          },
        ),
      ],
    );
  }
}




class BottomSheet extends StatefulWidget {
  @override
  _BottomSheetState createState() => _BottomSheetState();
}

class _BottomSheetState extends State<BottomSheet> {
  @override
  Widget build(BuildContext context) {
    List<Cat> cats = Provider.of<CatProvider>(context).cats;
    List<int> selectCats = Provider.of<EstablishmentProvider>(context).selectCats;
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.only(bottom: 20),
        child: Wrap(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Фильтр',style: TextStyle(fontSize: 18,fontWeight: FontWeight.w700),),
                  IconButton(onPressed: (){
                    Navigator.pop(context);
                  }, icon: Icon(Icons.close,color: purple,))
                ],
              ),
            ),
            for (var cat in cats)
              Container(
                margin: EdgeInsets.only(left: 10, top: 10),
                child: TextButton(
                  child: Text(
                    cat.name,
                    style: TextStyle(
                        color: cat.isSelect ? Colors.white : Colors.black),
                  ),
                  onPressed: () {
                    setState(() {
                      cat.isSelect = !cat.isSelect;
                      if  (selectCats.contains(cat.id)){
                        selectCats.remove(cat.id);
                      }else{
                        selectCats.add(cat.id);
                      }


                      Provider.of<EstablishmentProvider>(context,listen: false).search();
                    });
                  },
                  style: ButtonStyle(

                    backgroundColor: cat.isSelect
                        ? MaterialStateProperty.all<Color>(purple)
                        : MaterialStateProperty.all<Color>(Colors.white),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: cat.isSelect ? BorderSide(color: purple) : BorderSide(color: Colors.grey[200]),
                      ),
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

