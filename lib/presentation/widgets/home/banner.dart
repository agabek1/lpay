import 'package:flutter/material.dart';
import 'package:lpay/presentation/widgets/home/banner_item.dart';

class BannerHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 16,bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 16),
                  child:  Text(
                    "Как это работает?",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  height: 79,
                  margin: EdgeInsets.only(top: 10),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      BannerItem(),
                      BannerItem(),
                      BannerItem(),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
