import 'package:flutter/material.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/providers/city_provider.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:provider/provider.dart';

class HomeAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  _HomeAppBarState createState() => _HomeAppBarState();
}

class _HomeAppBarState extends State<HomeAppBar> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<City> cities = Provider.of<CityProvider>(context).cities;
    UserProvider userProvider = Provider.of<UserProvider>(context);
    City currentCity = userProvider.user != null ? userProvider.user.city : null;

    return AppBar(
      title: const Text(
        'Главная',
        style: TextStyle(color: Colors.black),
      ),
      actions: <Widget>[
        GestureDetector(
          onTap: (){
            showModalBottomSheet(
                context: context,
                builder: (context) {
                  return ListView.builder(
                      padding: const EdgeInsets.all(8),
                      itemCount: cities.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: (){
                            Provider.of<UserProvider>(context,listen: false).editCity(cities[index]);
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 50,
                            child: Center(child: Text(cities[index].name)),
                          ),
                        );
                      }
                  );
                });
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Align(
              alignment: Alignment.center,
              child: Row(
                children: [
                  Text(
                    currentCity != null ? currentCity.name : '',
                    style: TextStyle(color: purple, fontWeight: FontWeight.bold),
                  ),
                  Icon(
                    Icons.room,
                    color: purple,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
      backgroundColor: Colors.white,
    );
  }
}
