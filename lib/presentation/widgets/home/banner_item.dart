import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BannerItem extends StatefulWidget {

  @override
  _BannerItemState createState() => _BannerItemState();
}

class _BannerItemState extends State<BannerItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16),
      padding: EdgeInsets.only(left: 8),
      width: MediaQuery.of(context).size.width * 0.6,
      decoration: BoxDecoration(
          border: Border.all(color: Color.fromRGBO(148, 162, 171, 0.6)),
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              margin: EdgeInsets.only(right: 8),
              child: SvgPicture.asset('images/hashtag.svg')),
          Expanded(child: Text("Публикуй в соцсетях посты с хештегами"))
        ],
      ),
    );
  }
}
