import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:provider/provider.dart';

class EstablishmentMainInfo extends StatefulWidget {
  @override
  _EstablishmentMainInfoState createState() => _EstablishmentMainInfoState();
}

class _EstablishmentMainInfoState extends State<EstablishmentMainInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16),
      color: Colors.white,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            margin: EdgeInsets.only(bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Хэштеги",
                  style: TextStyle(fontSize: 16),
                ),
                Text(
                  context.watch<EstablishmentProvider>().establishment.hashtag,
                  style: TextStyle(color: purple),
                )
              ],
            ),
          ),
          _Carousel(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            margin: EdgeInsets.only(top: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 7.0),
                  child: Text(
                    "Описание",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                  context
                      .watch<EstablishmentProvider>()
                      .establishment
                      .description,
                  style: TextStyle(fontSize: 14),
                )
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            margin: const EdgeInsets.only(top: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 7),
                  child: Text(
                    "Контакты",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(Icons.phone, color: purple),
                    ),
                    Text(
                      context
                          .watch<EstablishmentProvider>()
                          .establishment
                          .phone,
                      style: TextStyle(fontSize: 14),
                    )
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 10),
                      child: Icon(Icons.computer, color: purple),
                    ),
                    Text(
                      context
                          .watch<EstablishmentProvider>()
                          .establishment
                          .website,
                      style: TextStyle(fontSize: 14),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Carousel extends StatefulWidget {
  @override
  __CarouselState createState() => __CarouselState();
}

class __CarouselState extends State<_Carousel> {
  @override
  Widget build(BuildContext context) {

    Establishment establishment = context.watch<EstablishmentProvider>().establishment;
    return CarouselSlider(
      options: CarouselOptions(
          viewportFraction: 1,
          aspectRatio: 16 / 9,
          autoPlay: false,
          autoPlayInterval: Duration(seconds: 8),
          autoPlayAnimationDuration: Duration(seconds: 3),
          autoPlayCurve: Curves.fastOutSlowIn,
          enlargeCenterPage: true),
      items: establishment.images.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 5.0),
                decoration: BoxDecoration(color: Colors.white),
                child: Image.network(serverUrl + i));
          },
        );
      }).toList(),
    );
  }
}
