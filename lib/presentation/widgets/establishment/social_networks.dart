import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:provider/provider.dart';



class SocialNetworks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.symmetric(vertical: 16),
      padding: EdgeInsets.symmetric(vertical: 16,horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              _like_price(),
              _logo(),
              _discount(),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SvgPicture.asset('images/icons/vk.svg',color: Color.fromRGBO(148, 162, 171, 0.69),),
                SvgPicture.asset('images/icons/twitter.svg',color: Color.fromRGBO(148, 162, 171, 0.69),),
                SvgPicture.asset('images/icons/instagram.svg',color: Color.fromRGBO(148, 162, 171, 0.69),),
                SvgPicture.asset('images/icons/facebook.svg',color: Color.fromRGBO(148, 162, 171, 0.69),),
              ],
            ),
          )
        ],
      ),
    );
  }
}


class _logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipOval(
          child: Container(
            color: Color.fromRGBO(241, 245, 247, 0.7),
            width: 72,
            height: 72,
            padding: EdgeInsets.all(15),
            child: SvgPicture.asset("images/default_logo.svg",color: Color.fromRGBO(148, 162, 171, 0.7),),
          ),
        ),
        Text("социальные сети")
      ],
    );
  }
}


class _like_price extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final int like = context.watch<EstablishmentProvider>().establishment.like;
    print('like like');
    return Column(
      children: [
        Row(
          children: [
            Icon(Icons.favorite,color: purple,),
            Text( like.toString() ,style: TextStyle(color: purple,fontSize: 24,fontWeight: FontWeight.bold),)
          ],
        ),
        Text('курс 1 лайк = 100')
      ],
    );
  }
}

class _discount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Icon(Icons.local_offer,color: purple,),
            Text("${context.watch<EstablishmentProvider>().establishment.discount.toString()}%",style: TextStyle(color: purple,fontSize: 24,fontWeight: FontWeight.bold),)
          ],
        ),
        Text("Скидка от чека")
      ],
    );
  }
}

