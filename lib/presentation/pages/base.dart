import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/presentation/pages/home_page.dart';
import 'package:lpay/presentation/pages/auth/login_page.dart';
import 'package:lpay/presentation/pages/profile_page.dart';
import 'package:lpay/presentation/pages/search_page.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/widgets/home/home_app_bar.dart';
import 'package:lpay/presentation/widgets/profile/profile_app_bar.dart';
import 'package:lpay/presentation/widgets/search/search_app_bar.dart';
import 'package:provider/provider.dart';

class Base extends StatefulWidget {
  @override
  _BaseState createState() => _BaseState();
}

class _BaseState extends State<Base> {
  @override
  int _index = 1;

  final List<Widget> _pages = <Widget>[
    SearchPage(),
    HomePage(),
    ProfilePage(),
  ];


  final List<PreferredSizeWidget> _appBars =  [
    SearchAppBar(),
    HomeAppBar(),
    ProfileAppBar(),
  ];



  void _onItemTapped(int index) {
    setState(() {
      _index = index;
    });
  }

  Widget build(BuildContext context) {

    return Consumer<UserProvider>(
      builder: (context, provider, child){
        if  (provider.status == Status.AuthError){
          Get.offAll(() => LoginPage());
        }
        return Scaffold(
            appBar:_appBars.elementAt(_index),
            bottomNavigationBar: ConvexAppBar(
              backgroundColor: Colors.white,
              color: purple,
              style: TabStyle.react,
              activeColor: purple,
              items: [
                TabItem(icon: Icons.search,title: 'Заведения'),
                TabItem(icon: Icons.home,title: 'Главная'),
                TabItem(icon: Icons.people,title: 'Профиль'),
              ],
              initialActiveIndex: _index,//opti,onal, default as 0
              onTap: _onItemTapped,
            ),
            body: _pages.elementAt(_index)
        );
      },
    );
  }
}
