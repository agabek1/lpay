import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/presentation/pages/add_post_page.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:lpay/presentation/widgets/establishment/establishment_info.dart';
import 'package:lpay/presentation/widgets/establishment/establishment_map.dart';
import 'package:lpay/presentation/widgets/establishment/social_networks.dart';
import 'package:provider/provider.dart';

class EstablishmentPage extends StatefulWidget {
  final int id;
  EstablishmentPage({@required this.id});

  @override
  _EstablishmentPageState createState() => _EstablishmentPageState();
}

class _EstablishmentPageState extends State<EstablishmentPage> {
  Establishment establishment;

  @override
  void initState() {
    EstablishmentProvider establishmentProvider =
    Provider.of<EstablishmentProvider>(context, listen: false);
    establishmentProvider.show(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    establishment = Provider.of<EstablishmentProvider>(context).establishment;

    return Consumer<EstablishmentProvider>(builder: (context, provider, child) {
      if (provider.load) {
        return Container(
          color: Colors.white,
          child: Stack(
            alignment: FractionalOffset.center,
            children: <Widget>[
              new CircularProgressIndicator(
                backgroundColor: Colors.red,
              ),
            ],
          ),
        );
      } else {
        Provider.of<PostProvider>(context, listen: false).establishment =
            provider.establishment;
        return Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            backgroundColor: Colors.white,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  provider.establishment.name,
                  style: TextStyle(fontSize: 18, color: Colors.black),
                ),
                Text(
                  provider.establishment.cat.name,
                  style: TextStyle(fontSize: 12, color: Colors.black),
                ),
              ],
            ),
          ),
          floatingActionButton: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () {
                String des = establishment.description + ' ' + establishment.hashtag;
                Get.to( () => AddPostPage(establishment.images[0], des));
              },
              child: Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: Text(
                  'Сделать пост',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(purple),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(35),
                    ),
                  ),
                ),
              ),
            ),
          ),
          floatingActionButtonLocation:
          FloatingActionButtonLocation.centerFloat,
          body: SingleChildScrollView(
            child: Column(
              children: [
                SocialNetworks(),
                EstablishmentMainInfo(),
                EstablishmentMap(provider.establishment)
              ],
            ),
          ),
        );
      }
    });
  }
}
