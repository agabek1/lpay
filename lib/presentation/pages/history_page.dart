import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/post.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:provider/provider.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  void initState() {
    Provider.of<PostProvider>(context, listen: false).getHistories();
    Provider.of<PostProvider>(context, listen: false).status =
        PostStatus.Loading;

    super.initState();
  }

  Future<void> _pullRefresh() async {
    Provider.of<PostProvider>(context, listen: false).getHistories();
    Provider.of<PostProvider>(context, listen: false).status = PostStatus.Loading;
  }

  @override
  Widget build(BuildContext context) {
    PostProvider postProvider = Provider.of<PostProvider>(context);
    TextEditingController instagramController = TextEditingController();
    if (postProvider.status == PostStatus.Success) {
      postProvider.status = PostStatus.InitPage;
    }

    if (postProvider.status == PostStatus.Loading) {
      return Container(
        color: Colors.white,
        child: Stack(
          alignment: FractionalOffset.center,
          children: <Widget>[
            new CircularProgressIndicator(
              backgroundColor: Colors.red,
            ),
          ],
        ),
      );
    } else {
      return RefreshIndicator(
        onRefresh: _pullRefresh,
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Истрория",
              style: TextStyle(color: Colors.black),
            ),
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            backgroundColor: Colors.white,
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(vertical: 13),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: postProvider.histories.length,
                      itemBuilder: (context, index) {
                        Post post = postProvider.histories[index];
                        return Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(bottom: 12),
                          child: Padding(
                            padding: EdgeInsets.all(13),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  post.createdAt,
                                  style: TextStyle(
                                      color: Color.fromRGBO(138, 139, 143, 1)),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 13),
                                  child: Row(
                                    children: [
                                      if (post.vkPostId != 'null')
                                        Container(
                                          margin: EdgeInsets.only(right: 7),
                                          child: SvgPicture.asset(
                                            'images/icons/vk.svg',
                                            color: purple,
                                          ),
                                        ),
                                      if (post.facebookLikes != null)
                                        Container(
                                          margin: EdgeInsets.only(right: 7),
                                          child: SvgPicture.asset(
                                            'images/icons/facebook.svg',
                                            color: purple,
                                          ),
                                        ),
                                      if (post.instagram != 'null')
                                        Container(
                                          margin: EdgeInsets.only(right: 7),
                                          child: SvgPicture.asset(
                                            'images/icons/instagram.svg',
                                            color: purple,
                                          ),
                                        ),
                                      if (post.twitter != null)
                                        Container(
                                          margin: EdgeInsets.only(right: 7),
                                          child: SvgPicture.asset(
                                            'images/icons/twitter.svg',
                                            color: purple,
                                          ),
                                        ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 13),
                                  child: Row(
                                    children: [
                                      Container(
                                        child: SvgPicture.asset(
                                          'images/default_logo.svg',
                                          color: Colors.red,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 12),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              postProvider.histories[index]
                                                  .establishment.cat.name,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12),
                                            ),
                                            Text(
                                              postProvider.histories[index]
                                                  .establishment.name,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 16),
                                  child: Row(
                                    children: [
                                      Icon(Icons.favorite),
                                      SizedBox(
                                        width: 7,
                                      ),
                                      Text(postProvider.histories[index].vkLikes
                                          .toString()),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text('='),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text(
                                          '${postProvider.histories[index].establishment.price}т'),
                                    ],
                                  ),
                                ),
                                if (post.instagram == 'null')
                                  
                                  Container(
                                    child: ElevatedButton(
                                      child: Text('Привязать ссылку Instagram'),
                                      onPressed: () {
                                        instagramController.text  = '';
                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(16.0),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Text(
                                                          'Добавить ссылку',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight.w700,
                                                              fontSize: 17),
                                                        ),
                                                        IconButton(
                                                            onPressed: () {
                                                              Get.back();
                                                            },
                                                            icon: Icon(
                                                              Icons.close,
                                                              color: purple,
                                                            ))
                                                      ],
                                                    ),
                                                    TextField(
                                                      controller: instagramController,
                                                      decoration: InputDecoration(
                                                        fillColor: Color.fromRGBO(
                                                            247, 247, 247, 1),
                                                        filled: true,
                                                        hintText:
                                                            'Вставьте ссылку поста',
                                                        border:
                                                            OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                            width: 0.5,
                                                            color: Color.fromRGBO(
                                                                148,
                                                                162,
                                                                171,
                                                                0.2),
                                                          ),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(50.0),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(height: 15,),
                                                    ElevatedButton(onPressed: ()  async {
                                                      if  (instagramController.text != null){
                                                        post.instagram = instagramController.text;
                                                        postProvider.instagramUrlAdd(postId: post.id,instagramUrl: instagramController.text);
                                                        _pullRefresh();
                                                        Get.back();

                                                      }
                                                    }, child: Text('Привязать'))
                                                  ],
                                                ),
                                              );
                                            });
                                      },
                                    ),
                                  )
                              ],
                            ),
                          ),
                        );
                      }),
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
}
