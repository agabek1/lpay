import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/auth/login_page.dart';
import 'package:lpay/presentation/pages/gid_page.dart';
import 'package:lpay/presentation/pages/history_page.dart';
import 'package:lpay/presentation/pages/privacy_policy_page.dart';
import 'package:lpay/presentation/pages/question_answer_page.dart';
import 'package:lpay/presentation/widgets/profile/profile_code.dart';
import 'package:lpay/presentation/widgets/profile/profile_info.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
class ProfilePage extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

const _url = 'https://api.whatsapp.com/send?phone=+77004448696';

void _launchURL() async => await canLaunch(_url)
    ? await launch(_url) : throw 'Not found $_url';

class _ProfileState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {

    final userProvider = Provider.of<UserProvider>(context);

    if (userProvider.status == Status.Logout){
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Get.offAll(() => LoginPage());
      });
    }

    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 10,bottom: 50),
        color: Colors.white,
        child: Column(
          children: [
              ProfileCode(),
              ProfileInfo(),
              Container(
                margin: EdgeInsets.only(left: 16,top: 40),
                height: 280,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Get.to( () => HistoryPage());
                      },

                      child: Row(
                        children: [
                          SvgPicture.asset('images/icons/history.svg'),
                          SizedBox(width: 14),
                          Text("История постов")
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Get.to( () => QuestionAnswerPage());
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset('images/icons/question_answers.svg'),
                          SizedBox(width: 14),
                          Text("Вопросы и ответы")
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Get.to(PrivacyPolicyPage());
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset('images/icons/privacy_policy.svg'),
                          SizedBox(width: 14),
                          Text("Политика конфиденциальности")
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Get.to(GidPage());
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset('images/icons/gid.svg'),
                          SizedBox(width: 14),
                          Text("Гид по приложению")
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: _launchURL,
                      child: Row(
                        children: [
                          SvgPicture.asset('images/icons/gid.svg'),
                          SizedBox(width: 14),
                          Text("напиши нам")
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Provider.of<UserProvider>(context,listen: false).logout();
                      },
                      child: Row(
                        children: [
                          SvgPicture.asset('images/icons/logout.svg'),
                          SizedBox(width: 14),
                          Text("Выход")
                        ],
                      ),
                    ),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
