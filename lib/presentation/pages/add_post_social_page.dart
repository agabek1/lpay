import 'dart:io';
import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/establishment_page.dart';
import 'package:lpay/presentation/widgets/add_post/add_post_main_info.dart';
import 'package:lpay/presentation/widgets/add_post/add_post_social.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as filePath;
import 'package:path_provider/path_provider.dart' as pathProvider;

class AddPostSocialPage extends StatefulWidget {
  final String imageUrl;
  final File image;
  final String des;
  const AddPostSocialPage({this.imageUrl,this.image, this.des});
  @override
  _AddPostSocialPageState createState() => _AddPostSocialPageState();
}

class _AddPostSocialPageState extends State<AddPostSocialPage> {
  // File _image;
  // XFile _file;
  String imageUrl;
  // final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    imageUrl = widget.imageUrl;
    // _fileFromImageUrl();
  }

  // Future<void> _fileFromImageUrl() async {
  //   try {
  //     String url = serverUrl + widget.imageUrl;
  //     final response = await http.get(Uri.parse(url));
  //     final documentDirectory = await pathProvider.getApplicationDocumentsDirectory();
  //     List<String> word = widget.imageUrl.split(".");
  //     String imageName = DateTime.now().millisecondsSinceEpoch.toString() +
  //         '.' +
  //         word[word.length - 1];
  //     setState(() {
  //       _file = XFile(
  //         filePath.join(
  //           documentDirectory.path,
  //           imageName,
  //         ),
  //       );
  //       _image = File(
  //         filePath.join(
  //           documentDirectory.path,
  //           imageName,
  //         ),
  //       );
  //       _image.writeAsBytesSync(response.bodyBytes);
  //     });
  //
  //     // print(_image.path);
  //   } catch (e, s) {
  //     print(e);
  //     print(s);
  //   }
  // }


  @override
  Widget build(BuildContext context) {
    PostProvider postProvider = Provider.of<PostProvider>(context);
    UserProvider userProvider = Provider.of<UserProvider>(context);

    // postProvider.image = _image;
    // postProvider.user = userProvider.user;

    if (postProvider.status == PostStatus.Success) {
      setState(() {
        imageUrl = postProvider.imageUrl;
      });
    }
    if (postProvider.status == PostStatus.Loading) {
      return Container(
        color: Colors.white,
        child: Stack(
          alignment: FractionalOffset.center,
          children: <Widget>[
            CircularProgressIndicator(
              backgroundColor: Colors.red,
            ),
          ],
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            'социальные сети',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              onPressed: () {
                  Get.offAll(EstablishmentPage(id: postProvider.establishment.id));
              },
              icon: Icon(
                Icons.done,
                color: purple,
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AddPostSocial(
                file: widget.image,
                imageUrl: serverUrl + imageUrl,
                des: widget.des,
              ),
            ],
          ),
        ),
      );
    }
  }
}
