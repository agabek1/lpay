import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:lpay/data/models/setting.dart';
import 'package:lpay/data/providers/setting_provider.dart';
import 'package:provider/provider.dart';


class GidPage extends StatefulWidget {
  @override
  _GidPageState createState() => _GidPageState();
}

class _GidPageState extends State<GidPage> {
  @override
  Widget build(BuildContext context) {
    Setting setting = Provider.of<SettingProvider>(context,listen: false).setting;
    return Scaffold(
      appBar: AppBar(
        title: Text("Гид по приложению",style: TextStyle(color: Colors.black),),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: HtmlWidget(setting.gid),
      ),
    );
  }
}
