import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/base.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:provider/provider.dart';

class RegisterVerificationPage extends StatefulWidget {
  @override
  _RegisterVerificationPageState createState() => _RegisterVerificationPageState();
}

class _RegisterVerificationPageState extends State<RegisterVerificationPage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
        builder: (context, provider, child){
          if (provider.status == Status.RegisterVerify){

            SchedulerBinding.instance.addPostFrameCallback((_) {
              // Navigator.of(context).popAndPushNamed("/");
              Get.offAll( () => Base());
            });
            // Navigator.of(context).pushAndRemoveUntil( MaterialPageRoute(builder: (context) => Base()),(Route<dynamic> route) => false);
          }

          if (provider.status == Status.Loader){
            return Container(
              color: Colors.white,
              child: Stack(
                alignment: FractionalOffset.center,
                children: <Widget>[
                  new CircularProgressIndicator(
                    backgroundColor: Colors.red,
                  ),
                ],
              ),
            );
          }else{
            return Scaffold(
              appBar: AppBar(
                title: Text('Регистрация',style: TextStyle(color: Colors.black),),
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(
                  color: Colors.black, //change your color here
                ),
              ),
              body: Center(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 50,vertical: 10),
                  child: PinFieldAutoFill(
                    codeLength: 4,
                    onCodeChanged: (code){
                      if (code.length == 4){
                        provider.registerVerify(code);
                      }
                    },
                  ),
                ),
              ),
            );

          }
        });
  }
}
