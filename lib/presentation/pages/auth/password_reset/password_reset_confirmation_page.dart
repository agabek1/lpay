import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/auth/password_reset/password_reset_page.dart';
import 'package:lpay/presentation/pages/auth/password_reset/password_reset_update_page.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class PasswordResetConfirmation extends StatefulWidget {
  final String phone;
  const PasswordResetConfirmation({this.phone});
  @override
  _PasswordResetConfirmationState createState() => _PasswordResetConfirmationState();
}

class _PasswordResetConfirmationState extends State<PasswordResetConfirmation> {
  TextEditingController codeController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
      builder: (context, userProvider, child) {
        print("userProvider.status: ${userProvider.status}");

        if (userProvider.status == Status.PasswordResetUpdate) {
            print('inner');
          // userProvider.status = Status.InitPage;
          SchedulerBinding.instance.addPostFrameCallback((_) {
            Get.to(() => PasswordResetUpdatePage(phone: widget.phone,code: codeController.text,));
          });
        }
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              "СМС код",
              style: TextStyle(color: Colors.black),
            ),
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 16),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 15, bottom: 8),
                        child: const Text(
                          "СМС код",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(fontSize: 16),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.3,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(255, 45, 45, 0.3),
                            ),
                          ),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Color.fromRGBO(247, 247, 247, 1),
                          // errorText: nameController.text.length > 1 ?   null:'Value Can\'t Be Empty',
                        ),
                        keyboardType: TextInputType.number,
                        // autofocus: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Введите код';
                          }
                          return null;
                        },

                        controller: codeController,
                      ),
                      if (userProvider.status == Status.Error)
                        Center(child: Text(userProvider.errorMessage)),
                      if (userProvider.status == Status.Loader)
                        Center(
                          child: CircularProgressIndicator(),
                        )
                      else
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          height: 48,
                          child: TextButton(
                              child: Text(
                                "подтвердить",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                backgroundColor: purple,
                              ),
                              onPressed: () {
                                if (!_formKey.currentState.validate()) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Пожалуйста, заполните все поля'),
                                    ),
                                  );
                                }else{
                                  print('click buuton');
                                  userProvider.passwordResetConfirmation(phone:widget.phone, code: codeController.text);
                                }
                              }),
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );

  }
}
