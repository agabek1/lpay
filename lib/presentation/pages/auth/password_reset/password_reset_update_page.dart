import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/auth/login_page.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class PasswordResetUpdatePage extends StatefulWidget {
  final String phone;
  final String code;
  PasswordResetUpdatePage({this.phone,this.code});

  @override
  _PasswordResetUpdatePageState createState() => _PasswordResetUpdatePageState();
}

class _PasswordResetUpdatePageState extends State<PasswordResetUpdatePage> {
  TextEditingController passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool passwordShow = false;
  @override

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    if (userProvider.status == Status.PasswordResetUpdateSuccess) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Get.offAll( () => LoginPage());
      });
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Новый пароль",
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 16),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 15, bottom: 8, top: 10),
                    child: const Text(
                      "Пароль",
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      hintStyle: TextStyle(fontSize: 16),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(
                          width: 0.3,
                          color: Color.fromRGBO(148, 168, 171, 0.3),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(
                          width: 0.4,
                          color: Color.fromRGBO(148, 162, 171, 0.33),
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(
                          width: 0.4,
                          color: Color.fromRGBO(255, 45, 45, 0.3),
                        ),
                      ),
                      filled: true,
                      contentPadding: EdgeInsets.all(16),
                      fillColor: Color.fromRGBO(247, 247, 247, 1),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            passwordShow = !passwordShow;
                          });
                        },
                        child: !passwordShow
                            ? const Icon(
                          Icons.remove_red_eye_rounded,
                          color: Colors.grey,
                        )
                            : const Icon(
                          Icons.visibility_off,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Введите пароль';
                      }
                      return null;
                    },
                    controller: passwordController,
                    obscureText: passwordShow,
                  ),
                  if (userProvider.status == Status.Error ||
                      userProvider.status == Status.AuthError)
                    Center(child: Text(userProvider.errorMessage)),
                  if (userProvider.status == Status.Loader)
                    Center(
                      child: CircularProgressIndicator(),
                    )
                  else
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      height: 48,
                      child: TextButton(
                          child: Text(
                            "подтвердить",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            backgroundColor: purple,
                          ),
                          onPressed: () {
                            if (!_formKey.currentState.validate()) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                      'Пожалуйста, заполните все поля'),
                                ),
                              );
                            }else{
                                userProvider.passwordResetUpdate(phone:widget.phone, code:widget.code,password: passwordController.text);
                            }
                          }),
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

