import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/auth/password_reset/password_reset_page.dart';
import 'package:lpay/presentation/pages/auth/register_page.dart';
import 'package:lpay/presentation/pages/base.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  bool passwordShow = true;
  final maskFormatter = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});
  final _formKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);

    if (userProvider.status == Status.Login) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Get.offAll(Base());
      });
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: [
                Image.asset('images/auth_bg.png'),
                const Positioned(
                  bottom: 30,
                  left: 16,
                  child: Text(
                    'С возвращением!',
                    style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    //Телефон номера
                    Container(
                      margin: const EdgeInsets.only(left: 15, bottom: 8),
                      child: const Text(
                        "Телефон номера",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        hintText: '+7 (702) 517-11-98',
                        hintStyle: TextStyle(fontSize: 16),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide(
                            width: 0.3,
                            color: Color.fromRGBO(148, 168, 171, 0.3),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide(
                            width: 0.4,
                            color: Color.fromRGBO(148, 168, 171, 0.3),
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide(
                            width: 0.4,
                            color: Color.fromRGBO(255, 45, 45, 0.3),
                          ),
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                        fillColor: Color.fromRGBO(247, 247, 247, 1),
                      ),
                      inputFormatters: [maskFormatter],
                      keyboardType: TextInputType.number,
                      controller: phoneController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Введите телефон номер';
                        }
                        return null;
                      },
                      //focus: true,
                    ),
                    //Пароль
                    Container(
                      margin: EdgeInsets.only(left: 15, bottom: 8, top: 20),
                      child: const Text(
                        "Пароль",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 16),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide(
                            width: 0.3,
                            color: Color.fromRGBO(148, 168, 171, 0.3),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide(
                            width: 0.4,
                            color: Color.fromRGBO(148, 162, 171, 0.33),
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide(
                            width: 0.4,
                            color: Color.fromRGBO(255, 45, 45, 0.3),
                          ),
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                        fillColor: Color.fromRGBO(247, 247, 247, 1),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              passwordShow = !passwordShow;
                            });
                          },
                          child: !passwordShow
                              ? const Icon(
                                  Icons.remove_red_eye_rounded,
                                  color: Colors.grey,
                                )
                              : const Icon(
                                  Icons.visibility_off,
                                  color: Colors.grey,
                                ),
                        ),
                      ),
                      controller: passwordController,
                      obscureText: passwordShow,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Введите пароль';
                        }
                        return null;
                      },
                    ),
                    //Забыл пароль
                    GestureDetector(
                      onTap: (){
                        Get.to( () => PasswordResetPage());
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 8, bottom: 32),
                        child: const Text(
                          "Забыл пароль. Восстановить",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              color: purple, fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),

                    if (userProvider.status == Status.Error || userProvider.status == Status.AuthError)
                      Center(child: Text(userProvider.errorMessage)),
                    //ВОЙТИ
                    if (userProvider.status == Status.Loader)
                      Center(
                        child: CircularProgressIndicator(),
                      )
                    else
                      Container(
                        margin: EdgeInsets.only(bottom: 16),
                        height: 48,
                        child: TextButton(
                            child: Text(
                              "ВОЙТИ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              backgroundColor: purple,
                            ),
                            onPressed: () {
                              if (!_formKey.currentState.validate()) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content:
                                        Text('Пожалуйста, заполните все поля'),
                                  ),
                                );
                              } else {
                                final String phone = phoneController.text
                                    .replaceAll(RegExp(r"[^0-9]+"), '');
                                final String password = passwordController.text;
                                Provider.of<UserProvider>(context,listen: false).login(phone, password);
                              }
                            }),
                      ),

                    GestureDetector(
                        onTap: () {
                          Get.to( () => RegisterPage());
                        },
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Все еще нет аккаунта?',
                                    style: TextStyle(color: Colors.black)),
                                TextSpan(
                                    text: ' Зарегистрироваться',
                                    style: TextStyle(color: purple)),
                              ],
                            ),
                          ),
                        ))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
