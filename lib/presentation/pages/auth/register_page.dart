import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/models/user.dart';
import 'package:lpay/data/providers/city_provider.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/auth/register_verification_page.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool passwordShow = true;
  String selectValue;

  int cityId;
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    context.read<CityProvider>().getCities();
    super.initState();
  }

  var maskFormatter = new MaskTextInputFormatter(
      mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    if (userProvider.status == Status.Register) {
      // Navigator.of(context).pushAndRemoveUntil(
      //     MaterialPageRoute(builder: (context) => Base()),
      //         (Route<dynamic> route) => false);
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Get.offAll( () => RegisterVerificationPage());
      });
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Регистрация",
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 15, left: 50, bottom: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Расскажите немного о себе"),
                    Text(
                        "Эта информация нужна для того,чтобы мы могли отправлять Вам интересные предложения"),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      //name
                      Container(
                        margin: const EdgeInsets.only(left: 15, bottom: 8),
                        child: const Text(
                          "Имя",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: '',
                          hintStyle: TextStyle(fontSize: 16),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.3,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(255, 45, 45, 0.3),
                            ),
                          ),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Color.fromRGBO(247, 247, 247, 1),
                          // errorText: nameController.text.length > 1 ?   null:'Value Can\'t Be Empty',
                        ),
                        keyboardType: TextInputType.text,
                        // autofocus: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Введите телефон номер';
                          }
                          return null;
                        },

                        controller: nameController,
                      ),
                      //Телефон номера
                      Container(
                        margin: const EdgeInsets.only(left: 15, bottom: 8),
                        child: const Text(
                          "Телефон номера",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: '+7 (702) 517-11-98',
                          hintStyle: TextStyle(fontSize: 16),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.3,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(255, 45, 45, 0.3),
                            ),
                          ),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Color.fromRGBO(247, 247, 247, 1),
                          // errorText: nameController.text.length > 1 ?   null:'Value Can\'t Be Empty',
                        ),
                        inputFormatters: [maskFormatter],
                        keyboardType: TextInputType.number,
                        // autofocus: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Введите телефон номер';
                          }
                          return null;
                        },

                        controller: phoneController,
                      ),
                      //Город
                      Container(
                        margin:
                            const EdgeInsets.only(left: 15, bottom: 8, top: 20),
                        child: const Text(
                          "Город",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      DropdownButtonFormField<String>(
                        value: selectValue,
                        decoration: const InputDecoration(
                          hintStyle: TextStyle(fontSize: 16),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.3,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(255, 45, 45, 0.3),
                            ),
                          ),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Color.fromRGBO(247, 247, 247, 1),
                        ),
                        items: context
                            .watch<CityProvider>()
                            .cities
                            .map((City city) {
                          return DropdownMenuItem<String>(
                            value: city.id.toString(),
                            child: Text(city.name),
                          );
                        }).toList(),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Выберите город';
                          }
                          return null;
                        },
                        onChanged: (e) {
                          cityId = int.parse(e);
                        },
                      ),
                      //email
                      Container(
                        margin:
                            const EdgeInsets.only(left: 15, bottom: 8, top: 20),
                        child: const Text(
                          "e-mail",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'myemail@gmail.com',
                          hintStyle: TextStyle(fontSize: 16),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.3,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(255, 45, 45, 0.3),
                            ),
                          ),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Color.fromRGBO(247, 247, 247, 1),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        // autofocus: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Введите почту';
                          }
                          return null;
                        },
                        controller: emailController,
                      ),
                      //password
                      Container(
                        margin: EdgeInsets.only(left: 15, bottom: 8, top: 20),
                        child: const Text(
                          "Пароль",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintStyle: TextStyle(fontSize: 16),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.3,
                              color: Color.fromRGBO(148, 168, 171, 0.3),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(148, 162, 171, 0.33),
                            ),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide: BorderSide(
                              width: 0.4,
                              color: Color.fromRGBO(255, 45, 45, 0.3),
                            ),
                          ),
                          filled: true,
                          contentPadding: EdgeInsets.all(16),
                          fillColor: Color.fromRGBO(247, 247, 247, 1),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                passwordShow = !passwordShow;
                              });
                            },
                            child: !passwordShow
                                ? const Icon(
                                    Icons.remove_red_eye_rounded,
                                    color: Colors.grey,
                                  )
                                : const Icon(
                                    Icons.visibility_off,
                                    color: Colors.grey,
                                  ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Введите пароль';
                          }
                          return null;
                        },
                        controller: passwordController,
                        obscureText: passwordShow,
                      ),
                      //Button
                      if (userProvider.status == Status.Error ||
                          userProvider.status == Status.AuthError)
                        Center(child: Text(userProvider.errorMessage)),
                      //ВОЙТИ

                      if (userProvider.status == Status.Loader)
                        Center(
                          child: CircularProgressIndicator(),
                        )
                      else
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          height: 48,
                          child: TextButton(
                              child: Text(
                                "Регистрация",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              style: TextButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                backgroundColor: purple,
                              ),
                              onPressed: () {
                                if (!_formKey.currentState.validate()) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Пожалуйста, заполните все поля'),
                                    ),
                                  );
                                }

                                final User user = User(
                                    phone: phoneController.text
                                        .replaceAll(RegExp(r"[^0-9]+"), ''),
                                    city: City(id: cityId),
                                    name: nameController.text,
                                    email: emailController.text,
                                    password: passwordController.text);

                                context.read<UserProvider>().register(user);
                                // if  (context.watch<UserProvider>().success){
                                //   // Navigator.push(
                                //   //   context,
                                //   //   MaterialPageRoute(builder: (context) {
                                //   //     return Base();
                                //   //   }),
                                //   // );
                                // }else{
                                //   //show error
                                // }
                              }),
                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//context.watch<CityProvider>().load.toString()
/*


 */
