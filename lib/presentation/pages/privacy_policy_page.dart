import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:lpay/data/models/setting.dart';
import 'package:lpay/data/providers/setting_provider.dart';
import 'package:provider/provider.dart';

class PrivacyPolicyPage extends StatefulWidget {
  @override
  _PrivacyPolicyPageState createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  @override
  Widget build(BuildContext context) {
    final Setting setting = Provider.of<SettingProvider>(context, listen: false).setting;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Политика конфиденциальности",
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: HtmlWidget(setting.privacyPolicy),
      ),
    );
  }
}
