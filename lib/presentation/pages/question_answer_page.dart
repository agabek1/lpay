import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:lpay/data/models/setting.dart';
import 'package:lpay/data/providers/setting_provider.dart';
import 'package:provider/provider.dart';

class QuestionAnswerPage extends StatefulWidget {
  @override
  _QuestionAnswerPageState createState() => _QuestionAnswerPageState();
}

class _QuestionAnswerPageState extends State<QuestionAnswerPage> {
  @override
  Widget build(BuildContext context) {
    final Setting setting = Provider.of<SettingProvider>(context, listen: false).setting;
    return Scaffold(
      appBar: AppBar(
        title: Text("Вопрос и ответы",style: TextStyle(color: Colors.black),),
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: HtmlWidget(setting.questionAnswer),
      ),
    );
  }
}
