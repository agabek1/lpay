import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:lpay/presentation/widgets/establishment_min.dart';
import 'package:lpay/presentation/widgets/home/banner.dart';

import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePage> {
  @override
  void initState() {
    context.read<EstablishmentProvider>().getList();
    

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          BannerHome(),
          Consumer<EstablishmentProvider>(builder: (context, provider, child) {
            if (provider.load) {
              return Stack(
                alignment: FractionalOffset.center,
                children: <Widget>[
                  new CircularProgressIndicator(
                    backgroundColor: Colors.red,
                  ),
                ],
              );
            } else {
              return Container(
                color: Colors.white,
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text(
                      "Популярные заведения",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    Container(
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: provider.establishments.length,
                          itemBuilder: (context, index) {
                            return EstablishmentMin(
                                establishment:
                                    provider.establishments.elementAt(index));
                          }),
                    ),
                  ],
                ),
              );
            }
          }),
        ],
      ),
    );
  }
}
