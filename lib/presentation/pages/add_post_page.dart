import 'dart:io';
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:lpay/presentation/pages/add_post_social_page.dart';
import 'package:lpay/presentation/widgets/add_post/add_post_main_info.dart';
import 'package:lpay/presentation/widgets/add_post/add_post_social.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as filePath;
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:permission_handler/permission_handler.dart';

class AddPostPage extends StatefulWidget {
  final String imageUrl;
  final String des;

  const AddPostPage(this.imageUrl, this.des);

  @override
  _AddPostPageState createState() => _AddPostPageState();
}

class _AddPostPageState extends State<AddPostPage> {
  File _image;
  final picker = ImagePicker();

  Future getImage(BuildContext context) async {
    if(await Permission.storage.request().isGranted) {
      final XFile pickedFile = await picker.pickImage(
          source: ImageSource.gallery);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          Provider
              .of<PostProvider>(context, listen: false)
              .image = _image;
        } else {
          print('No image selected.');
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final PostProvider postProvider = Provider.of<PostProvider>(context);
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    TextEditingController textController = TextEditingController();
    postProvider.image = _image;
    postProvider.user = userProvider.user;


    if (postProvider.status == PostStatus.Loading) {
      return Container(
        color: Colors.white,
        child: Stack(
          alignment: FractionalOffset.center,
          children: <Widget>[
            CircularProgressIndicator(
              backgroundColor: Colors.red,
            ),
          ],
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: purple, //change your color here
          ),
          title: Text(
            'Новый пост',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          actions: [
            IconButton(
              onPressed: () {
                if (_image != null) {
                  postProvider.status = PostStatus.Loading;
                  postProvider.body = textController.text;
                  postProvider.user = userProvider.user;
                  postProvider.add();
                  Get.to( () => AddPostSocialPage(imageUrl: postProvider.imageUrl,image:_image,des: postProvider.establishment.hashtag ,));
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Пожалуйста, выберите фотографию'),
                    ),
                  );
                }
              },
              icon: Icon(
                Icons.done,
                color: purple,
              ),
            )
          ],
        ),
        floatingActionButton: ClipOval(
          child: Container(
              // color: Colors.white,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: purple,
                  offset: const Offset(
                    5.0,
                    5.0,
                  ),
                  blurRadius: 10.0,
                  spreadRadius: 2.0,
                ), //BoxShadow
                BoxShadow(
                  color: Colors.white,
                  offset: const Offset(0.0, 0.0),
                  blurRadius: 0.0,
                  spreadRadius: 0.0,
                ), //Box
              ]),
              child: IconButton(
                onPressed: () => getImage(context),
                icon: Icon(
                  Icons.add_a_photo,
                  color: purple,
                  size: 28,
                ),
              )),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AddPostMainInfo(),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                padding: EdgeInsets.symmetric(horizontal: 16),
                color: Colors.white,
                child: TextField(
                  controller: textController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Отзыв',
                  ),
                ),
              ),
              Center(
                child: _image == null ? null : Image.file(_image),
              )
            ],
          ),
        ),
      );
    }
  }
}
