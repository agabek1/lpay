import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lpay/config/colors.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:lpay/presentation/widgets/establishment_min.dart';
import 'package:provider/provider.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<SearchPage> {
  @override
  void initState() {
    // Provider.of<EstablishmentProvider>(context,listen: false).getList();
    context.read<EstablishmentProvider>().getList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // EstablishmentProvider establishmentProvider = Provider.of<EstablishmentProvider>(context);
    // List<Establishment> establishments = establishmentProvider.establishments;

    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _searchBar(),
          Consumer<EstablishmentProvider>(builder: (context, provider, child) {
            if (provider.load) {
              return Container(
                color: Colors.white,
                child: Stack(
                  alignment: FractionalOffset.center,
                  children: <Widget>[
                    new CircularProgressIndicator(
                      backgroundColor: Colors.red,
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Популярные заведения",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: provider.establishments.length,
                        itemBuilder: (context, index) {
                          return EstablishmentMin(
                              establishment: provider.establishments.elementAt(index));
                        })
                  ],
                ),
              );
            }
          }),
        ],
      ),
    );
  }
}

class _searchBar extends StatefulWidget {
  @override
  __searchBarState createState() => __searchBarState();
}

class __searchBarState extends State<_searchBar> {
  @override
  Widget build(BuildContext context) {
    EstablishmentProvider establishmentProvider =
        Provider.of<EstablishmentProvider>(context, listen: false);
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 12),
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              prefixIcon: const Icon(Icons.search),
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              hintText: "Поиск заведении",
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 1.0),
                  borderRadius: BorderRadius.circular(25.0)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: purple, width: 1.0),
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
            onChanged: (e) {
              setState(() {
                establishmentProvider.searchText = e;
                establishmentProvider.search();
              });
            },
          ),
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 10),
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      establishmentProvider.sort = 'discount';
                      establishmentProvider.search();
                    });
                  },
                  child: Text(
                    'Больше скидок',
                    style: TextStyle(
                        color: establishmentProvider.sort == 'discount'
                            ? Colors.white
                            : Colors.black),
                  ),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          establishmentProvider.sort == 'discount'
                              ? purple
                              : Colors.white),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.grey)))),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 10),
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      establishmentProvider.sort = 'id';
                      establishmentProvider.search();
                    });
                  },
                  child: Text(
                    'Новые',
                    style: TextStyle(
                        color: establishmentProvider.sort == 'id'
                            ? Colors.white
                            : Colors.black),
                  ),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          establishmentProvider.sort == 'id'
                              ? purple
                              : Colors.white),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.grey)))),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
