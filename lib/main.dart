import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lpay/prefs.dart';
import 'package:lpay/presentation/pages/auth/login_page.dart';
import 'package:lpay/presentation/pages/base.dart';
import 'package:lpay/data/providers/cat_provider.dart';
import 'package:lpay/data/providers/city_provider.dart';
import 'package:lpay/data/providers/establishment_provider.dart';
import 'package:lpay/data/providers/post_provider.dart';
import 'package:lpay/data/providers/setting_provider.dart';
import 'package:lpay/data/providers/user_provier.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  // SharedPreferences.setMockInitialValues({});
  WidgetsFlutterBinding.ensureInitialized();
  await PreferenceUtils.init();
  runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => PostProvider()),
          ChangeNotifierProvider(create: (_) => CatProvider()),
          ChangeNotifierProvider(create: (_) => CityProvider()),
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => EstablishmentProvider()),
          ChangeNotifierProvider(create: (_) => SettingProvider()),
        ],
        child: GetMaterialApp(home: MyApp(),),
      ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget initPage(BuildContext context) {
    final String accessToken = PreferenceUtils.getString('accessToken');
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    if (accessToken == '') {
      return LoginPage();
    } else {
      if (userProvider.status != Status.Loader) {
        userProvider.profile(accessToken);
      }
      //
      if (userProvider.status == Status.Login) {
        Provider.of<PostProvider>(context, listen: false).checkInstagram();
        return Base();
      } else {
        return LoginPage();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CatProvider>(context, listen: false).getCats();
    Provider.of<CityProvider>(context, listen: false).getCities();
    Provider.of<SettingProvider>(context, listen: false).getSetting();

    return MaterialApp(
      title: 'LPlay',
      theme: ThemeData(
          fontFamily: 'Loto',
          scaffoldBackgroundColor: Color.fromRGBO(245, 245, 245, 1.0)),
      debugShowCheckedModeBanner: false,
      home: initPage(context),
    );
  }
}
