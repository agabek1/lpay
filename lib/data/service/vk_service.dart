import 'dart:convert';
import 'package:http/http.dart' as http;
class VkService {
  final String vkAccessToken;
  final String imagePath;
  final String hashtag;

  VkService({ this.imagePath,this.vkAccessToken,this.hashtag});

  Future<String> addPost() async {
    final String _photoUploadUrl = await _vkGetUploadUrl();
    final Map<String, dynamic> _photos = await _vkUploadPhoto(imagePath, _photoUploadUrl);
    final Map<String, dynamic> _id = await _vkPhotoSave(
        photo: _photos['photo'].toString(),
        hash: _photos['hash'].toString(),
        server: _photos['server'].toString());

    final response = await _addPost(photos: _id['response'] as List);

    return response['response']['post_id'].toString();
  }

  Future<String> _vkGetUploadUrl() async {
    final String method_url ='https://api.vk.com/method/photos.getWallUploadServer?v=5.52&access_token=$vkAccessToken';
    final uri = Uri.parse(method_url);
    final response = await http.get(uri);
    final body = jsonDecode(response.body);
    return body['response']['upload_url'].toString();
  }

  Future<Map<String, dynamic>> _vkUploadPhoto(
      String imagePath, String uploadUrl) async {
    final request = http.MultipartRequest("POST", Uri.parse(uploadUrl));

    final http.MultipartFile multipartFile =
    await http.MultipartFile.fromPath('photo', imagePath);

    request.files.add(multipartFile);

    final http.Response response =
    await http.Response.fromStream(await request.send());

    return jsonDecode(response.body) as Map<String, dynamic>;
  }

  Future<Map<String, dynamic>> _vkPhotoSave({String photo, String hash, String server}) async {
    final String url = 'https://api.vk.com/method/photos.saveWallPhoto?server=$server&v=5.52&access_token=$vkAccessToken&hash=$hash&photo=$photo';
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    final body = jsonDecode(response.body) as Map<String, dynamic>;
    return body;
  }

  Future<Map<String, dynamic>> _addPost({List<dynamic> photos}) async {
    final uri = Uri.parse('https://api.vk.com/method/wall.post');
    final response = await http.post(uri, body: {
      "access_token": vkAccessToken,
      "v": "5.52",
      "attachments": "photo${photos[0]['owner_id']}_${photos[0]['id']}",
      "message": hashtag
    });
    return jsonDecode(response.body) as Map<String, dynamic>;
  }
}
