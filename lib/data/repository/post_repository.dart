import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/instagram_post.dart';
import 'package:lpay/data/models/post.dart';
import 'package:lpay/data/models/user.dart';
import 'package:lpay/prefs.dart';

class PostRepository {
  Future<dynamic> add(
      {String accessToken,
      String body,
      File image,
      int establishmentId}) async {
    final request =
        http.MultipartRequest("POST", Uri.parse(apiUrl + '/post/add'));

    final http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath('image', image.path.toString());

    request.headers['Authorization'] = "Bearer $accessToken";
    request.files.add(multipartFile);
    request.fields['establishment_id'] = establishmentId.toString();
    request.fields['body'] = body;

    final http.Response response =
        await http.Response.fromStream(await request.send());
    if (response.statusCode == 200) return json.decode(response.body);
  }

  Future<void> update({String accessToken, int postId, String vkPostId,String instagramUrl,String instagramPage,String instagramLikes}) async {
    Map<String,String> params = {"post_id":postId.toString()};
    if  (vkPostId != null ){
      params.addAll({"vk_post_id":vkPostId.toString()});
    }
    if (instagramUrl != null ){
      params.addAll({"instagram":instagramUrl});
    }
    if (instagramPage != null ){
      params.addAll({"instagram_page":instagramPage});
    }
    if (instagramLikes != null ){
      params.addAll({"instagram_likes":instagramLikes});
    }
    await http.post(Uri.parse('${apiUrl}/post/update'),
        headers: {'Authorization': "Bearer $accessToken"},
        body: params
    );
  }

  Future<List<Post>> getHistories(User user) async {
    final String accessToken = PreferenceUtils.getString('accessToken');
    final uri = Uri.http(IP, 'api/v1/post/histories');
    Map<String, String> headers = {
      "Content-Type": 'application/json',
      "Authorization": "Bearer $accessToken"
    };
    final response = await http.get(uri, headers: headers);
    final data = jsonDecode(response.body);

    final List<Post> posts = (data as List).map((i) => Post.fromJson(i)).toList();
    return posts;
  }
  Future<List<InstagramPost>> lastInstagramPosts() async {
    final String accessToken = PreferenceUtils.getString('accessToken');
    final uri = Uri.http(IP, 'api/v1/post/instagram-urls');
    Map<String, String> headers = {
      "Content-Type": 'application/json',
      "Authorization": "Bearer $accessToken"
    };
    final response = await http.get(uri, headers: headers);
    final data = jsonDecode(response.body);
    final List<InstagramPost> posts  =( data as List).map((i) => InstagramPost.fromJson(i)).toList();
    return posts;
  }
  Future<String> getInstagramPage(String url) async {
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    return response.body;
  }
}


