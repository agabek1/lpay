import 'dart:convert';

import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/models/city.dart';
import 'package:http/http.dart' as http;
import 'package:lpay/data/models/setting.dart';

class SettingRepository {
  Future<Setting> getSetting() async {
    final response = await http.get(Uri.parse('${apiUrl}/setting'));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      return Setting(
        privacyPolicy: data['privacy_policy'].toString(),
        gid: data['gid'].toString(),
        whatsapp: data['whatsapp'].toString(),
        questionAnswer: data['question_answer'].toString(),
      );
    } else {
      throw NetworkException();
    }
  }
}

class NetworkException implements Exception {}

