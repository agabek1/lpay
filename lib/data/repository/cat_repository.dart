import 'dart:convert';

import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/models/city.dart';
import 'package:http/http.dart' as http;

class CatRepository {
  Future<List<Cat>> getCats() async {
    final response = await http.get(Uri.parse('${apiUrl}/cats'));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final List<Cat> cats = (data as List)
          .map(
            (i) => Cat(
              id: int.parse(i['id'].toString()),
              name: i['name'].toString(),
            ),
          )
          .toList();
      return cats;
    } else {
      throw NetworkException();
    }
  }
}

class NetworkException implements Exception {}

class PhoneAlreadyExists implements Exception {}
