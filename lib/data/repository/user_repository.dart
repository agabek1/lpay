import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/city.dart';
import 'package:http/http.dart' as http;
import 'package:lpay/data/models/user.dart';

class UserRepository {
  Future<User> registerValid(String phone, String code) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/verification'),
      headers: {"Content-type": "application/json"},
      body: jsonEncode(<String, String>{
        'phone': phone,
        'code': code,
      }),
    );
    final data = jsonDecode(response.body);

    switch (response.statusCode) {
      case 200:
        final User user = User(
          id: int.parse(data['user']['id'].toString()),
          accessToken: data['accessToken'].toString(),
          name: data['user']['name'].toString(),
          email: data['user']['email'].toString(),
          phone: data['user']['phone'].toString(),
          city: City(
              id: int.parse(data['user']['city']['id'].toString()),
              name: data['user']['city']['name'].toString()
          ),
          avatar: data['user']['avatar'].toString(),
          vkAccessToken: data['user']['vk_access_token'].toString(),
          vkId: data['user']['vk_id'].toString(),
          twitter: data['user']['twitter'].toString(),
          facebookUserId: data['user']['facebook_user_id'].toString(),
          facebookAccessToken: data['user']['facebook_access_token'].toString(),
          instagram: data['user']['instagram'].toString(),
        );
        return user;
        break;
      case 400:
        print("user register status code : ${response.statusCode}");
        throw ErrorException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }

    throw ErrorException(message: 'H');
  }

  Future register(User user) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/register'),
      headers: {"Content-type": "application/json"},
      body: jsonEncode(<String, String>{
        'name': user.name,
        'phone': user.phone,
        'password': user.password,
        'email': user.email,
        'city_id': user.city.id.toString(),
      }),
    );
    final data = jsonDecode(response.body);

    switch (response.statusCode) {
      case 200:
        break;
      case 400:
        print("user register status code : ${response.statusCode}");
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }
  }

  Future<User> login(String phone, String password) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/login'),
      headers: {"Content-type": "application/json"},
      body: jsonEncode(<String, String>{
        'phone': phone,
        'password': password,
      }),
    );
    final data = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        final User user = User(
          id: int.parse(data['user']['id'].toString()),
          accessToken: data['accessToken'].toString(),
          name: data['user']['name'].toString(),
          email: data['user']['email'].toString(),
          phone: data['user']['phone'].toString(),
          city: City(
            id: int.parse(data['user']['city']['id'].toString()),
            name: data['user']['city']['name'].toString()
          ),
          avatar: data['user']['avatar'].toString(),
          vkAccessToken: data['user']['vk_access_token'].toString(),
          vkId: data['user']['vk_id'].toString(),
          twitter: data['user']['twitter'].toString(),
          facebookUserId: data['user']['facebook_user_id'].toString(),
          facebookAccessToken: data['user']['facebook_access_token'].toString(),
          instagram: data['user']['instagram'].toString(),
        );
        return user;
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }

    throw NetworkException();
  }

  Future<User> profile(String accessToken) async {
    final response = await http.get(
      Uri.parse('${apiUrl}/profile'),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $accessToken"
      },
    );
    final data = jsonDecode(response.body);
    print(data);
    switch (response.statusCode) {
      case 200:
        final User user = User(
          id: int.parse(data['id'].toString()),
          accessToken: accessToken,
          name: data['name'].toString(),
          email: data['email'].toString(),
          phone: ['phone'].toString(),
          city: City(
              id: int.parse(data['city']['id'].toString()),
              name: data['city']['name'].toString()
          ),
          avatar: data['avatar'].toString(),
          vkAccessToken: data['vk_access_token'].toString(),
          vkId: data['vk_id'].toString(),
          twitter: data['twitter'].toString(),
          facebookUserId: data['facebook_user_id'].toString(),
          facebookAccessToken: data['facebook_access_token'].toString(),
          instagram: data['instagram'].toString(),
        );
        return user;
        break;
      case 401:
        print("Unauthenticated : ${response.statusCode}");
        throw AuthException();
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }

    throw NetworkException();
  }

  Future<User> edit({Map<String,dynamic> body, String accessToken}) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/edit'),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $accessToken"
      },
      body: jsonEncode(body),
    );
    final data = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        final User user = User.fromJson(data);
        return user;
        break;
      case 401:
        print("Unauthenticated : ${response.statusCode}");
        throw AuthException();
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }

    throw NetworkException();
  }

  Future<User> editCity(int cityId,String accessToken) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/edit/city'),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $accessToken"
      },
      body: jsonEncode(<String, String>{
        'city_id': cityId.toString(),
      }),
    );
    final data = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        User user = User.fromJson(data);
        return user;
        break;
      case 401:
        print("Unauthenticated : ${response.statusCode}");
        throw AuthException();
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }

    throw NetworkException();
  }

  Future<void> passwordReset({String phone,String accessToken}) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/password-reset'),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $accessToken"
      },
      body: jsonEncode(<String, String>{
        'phone': phone.toString(),
      }),
    );
    final data = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        break;
      case 401:
        print("Unauthenticated : ${response.statusCode}");
        throw AuthException();
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
      default:
        throw NetworkException();
        break;
    }

  }

  Future<void> passwordResetConfirmation({String phone,String code,String accessToken}) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/password-reset-confirmation'),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $accessToken",
        'Accept':'application/json'
      },
      body: jsonEncode(<String, String>{
        'phone': phone,
        'code': code,
      }),
    );
    final data = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        break;
      case 401:
        print("Unauthenticated : ${response.statusCode}");
        throw AuthException();
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        print(data);
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
      default:
        throw NetworkException();
        break;
    }
  }

  Future<void> passwordResetUpdate({String phone,String password,String code, String accessToken}) async {
    final response = await http.post(
      Uri.parse('${apiUrl}/password-reset-update'),
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer $accessToken"
      },
      body: jsonEncode(<String, String>{
        'phone': phone,
        'code': code,
        'password': password,
      }),
    );
    final data = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        break;
      case 401:
        print("Unauthenticated : ${response.statusCode}");
        throw AuthException();
        break;
      case 400:
        print("user login status code : ${response.statusCode}");
        print('asdas = ${response.body.toString()}');
        throw UserNotFoundException(message: data.toString());
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
      default:
        throw NetworkException();
        break;
    }

  }
}

class ErrorException implements Exception {
  String message;

  ErrorException({@required this.message});
}

class NetworkException implements Exception {}

class AuthException implements Exception {}

class PhoneAlreadyExists implements Exception {}

class UserNotFoundException implements Exception {
  String message;

  UserNotFoundException({@required this.message});
}

class ServerException implements Exception {}
