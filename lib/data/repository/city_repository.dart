import 'dart:convert';

import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/city.dart';
import 'package:http/http.dart' as http;

class CityRepository {
  Future<List<City>> getCities() async {
    final response = await http.get(Uri.parse('${apiUrl}/cities'));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      print(data);
      final List<City> cities = (data as List)
          .map(
            (i) => City(
              id: int.parse(i['id'].toString()),
              name: i['name'].toString(),
            ),
          )
          .toList();
      return cities;
    } else {
      throw NetworkException();
    }
  }
}

class NetworkException implements Exception {}

class PhoneAlreadyExists implements Exception {}
