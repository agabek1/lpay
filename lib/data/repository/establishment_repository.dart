import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/prefs.dart';

class EstablishmentRepository {
  Future<List<Establishment>> index(
      String sort, String search, List<int> cats) async {
    Map<String, String> queryParameters = {};
    if (sort != null) {
      queryParameters['sort'] = sort;
    }
    if (search != null) {
      queryParameters['search'] = search;
    }
    if (cats.length > 0) {
      for (var i = 0; i < cats.length; i++) {
        queryParameters['cats[$i]'] = cats[i].toString();
      }
    }
    final String accessToken = PreferenceUtils.getString('accessToken');
    final uri = Uri.http(IP, 'api/v1/establishments', queryParameters);
    Map<String, String> headers = {
      "Content-Type": 'application/json',
      "Authorization": "Bearer $accessToken"
    };
    final response = await http.get(uri, headers: headers);

    //
    // String queryString = Uri(queryParameters: queryParameters).query;
    //
    // var requestUrl = apiUrl + '/establishments' + '?' + queryString; // result - https://www.myurl.com/api/v1/user?param1=1&param2=2
    // var response = await http.get(Uri.parse(requestUrl));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final List<Establishment> etablishments = (data as List)
          .map(
            (i) => Establishment.fromJson(i),
          )
          .toList();
      return etablishments;
    } else {
      throw NetworkException();
    }
  }

  Future<Establishment> show(int id) async {

    final String accessToken = PreferenceUtils.getString('accessToken');
    final response = await http.get(
        Uri.parse('${apiUrl}/establishment/show?id=${id.toString()}'),
        headers: {"Content-type": "application/json", "Authorization": "Bearer $accessToken"});
    final data = json.decode(response.body);
    switch (response.statusCode) {
      case 200:
        Establishment establishment = Establishment.fromJson(data);
        return establishment;
        break;
      case 500:
        print("user login status code : ${response.statusCode}");
        throw ServerException();
        break;
    }

    throw NetworkException();
  }
}

class NetworkException implements Exception {}

class PhoneAlreadyExists implements Exception {}

class UserNotFoundException implements Exception {
  String message;

  UserNotFoundException({@required this.message});
}

class ServerException implements Exception {}
