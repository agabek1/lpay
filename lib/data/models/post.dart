import 'dart:convert';
import 'dart:io';

import 'package:lpay/config/settings.dart';
import 'package:lpay/data/models/establishment.dart';

class Post {

  int id;
  int userId;
  String body;
  File image;
  String urlImage;
  Establishment establishment;

  String vkPostId;
  int vkLikes;

  int facebookLikes;

  String instagram;
  int instagramLikes;

  String twitter;
  String createdAt;

  Post({
      this.id,
      this.userId,
      this.body,
      this.image,
      this.urlImage,
      this.establishment,

      this.vkPostId,
      this.vkLikes,
      this.facebookLikes,
      this.instagramLikes,

      this.instagram,
      this.twitter,
      this.createdAt,
    });
  factory Post.fromJson(json) {
    // Uri uri = Uri.parse(IP+ '/' + json['image'].toString());
    return Post(
      id: json['id'] as int,
      userId: json['user_id'] as int,
      body: json['body'] as String,
      urlImage: json['image'] as String,
      establishment: Establishment.fromJson(json['establishment']),
      vkPostId: json['vk_post_id'].toString(),
      vkLikes: json['vk_likes'] as int,
      facebookLikes: json['facebook_likes'] as int,
      instagramLikes: json['instagram_likes'] as int,
      instagram: json['instagram'].toString(),
      createdAt: json['created_at'].toString(),
    );
  }

}
