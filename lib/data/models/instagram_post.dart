class InstagramPost{
  final int postId;
  final String url;

  InstagramPost({this.postId,this.url});


  factory InstagramPost.fromJson(json) {
    return InstagramPost(
      postId: json['id'] as int,
      url: json['instagram'] as String,
    );
  }
}