

import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/models/city.dart';

class Establishment {
  int id;
  Cat cat;
  City city;
  String logo;
  String name;
  String phone;
  String website;
  String description;
  String address;
  double lat;
  double lng;
  String hashtag;
  int discount;
  int price;
  int like;
  String vkontakte;
  String instagram;
  String facebook;
  String twitter;
  DateTime created_at;
  List<String> images;
  bool isSelect;

  Establishment(
      {this.id,
      this.cat,
      this.city,
      this.logo,
      this.name,
      this.phone,
      this.website,
      this.description,
      this.address,
      this.lat,
      this.lng,
      this.hashtag,
      this.discount,
      this.price,
      this.like,
      this.vkontakte,
      this.instagram,
      this.facebook,
      this.twitter,
      this.created_at,
      this.images,
      this.isSelect});

  factory Establishment.fromJson(json) {
    return Establishment(
        id: int.parse(json['id'].toString()),
        cat: Cat(
            id: int.parse(json['cat']['id'].toString()),
            name: json['cat']['name'].toString()),
        city: City(
            id: int.parse(json['cat']['id'].toString()),
            name: json['cat']['name'].toString()),
        logo: json['logo'].toString(),
        name: json['name'].toString(),
        phone: json['phone'].toString(),
        website: json['website'].toString(),
        description: json['description'].toString(),
        address: json['address'].toString(),
        lat: double.parse(json['lat'].toString()),
        lng: double.parse(json['lng'].toString()),
        hashtag: json['hashtag'].toString(),
        discount: int.parse(json['discount'].toString()),
        price: int.parse(json['price'].toString()),
        like: int.parse(json['like'].toString()),
        vkontakte: json['vkontakte'].toString(),
        instagram: json['instagram'].toString(),
        facebook: json['facebook'].toString(),
        twitter: json['twitter'].toString(),
        created_at: DateTime.parse(json['created_at'].toString()),
        images: (json['images'] as List).map((e) {
          return e.toString();
        }).toList());
  }
}
