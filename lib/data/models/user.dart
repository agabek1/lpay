


import 'package:lpay/data/models/city.dart';

class User {
   int id;
   String name;
   String phone;
   String accessToken;
   String email;
   String password;
   String avatar;

   String vkAccessToken;
   String vkId;
   String instagram;
   String facebookAccessToken;
   String facebookUserId;
   String twitter;

   City city;

   User({this.id,this.name,this.phone,this.accessToken,this.email,this.password,this.avatar,this.vkAccessToken,this.vkId,this.instagram,this.city,this.facebookAccessToken,this.facebookUserId,this.twitter});

   factory User.fromJson(json) =>
       User(
          id: int.parse(json["id"].toString()),
          name: json["name"].toString(),
          phone: json["phone"].toString(),
          accessToken: json["accessToken"].toString(),
          email: json["email"].toString(),
           city: City(
               id: int.parse(json['city']['id'].toString()),
               name: json['city']['name'].toString()
           ),
          avatar: json["avatar"].toString(),
          vkAccessToken: json["vk_access_token"].toString(),
          vkId: json["vk_id"].toString(),
          instagram: json["instagram"].toString(),
          facebookAccessToken: json["facebook_access_token"].toString(),
          facebookUserId: json["facebook_user_id"].toString(),
          twitter: json["twitter"].toString(),
       );


}
