import 'package:flutter/cupertino.dart';
import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/models/setting.dart';
import 'package:lpay/data/repository/cat_repository.dart';
import 'package:lpay/data/repository/city_repository.dart';
import 'package:lpay/data/repository/setting_repository.dart';

class SettingProvider extends ChangeNotifier{
  Setting _setting;

  Setting get setting => _setting;


  void getSetting () async{
    _setting = await SettingRepository().getSetting();
    notifyListeners();
  }
}