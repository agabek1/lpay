import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/data/repository/city_repository.dart';
import 'package:lpay/data/repository/establishment_repository.dart';

class EstablishmentProvider extends ChangeNotifier{
  bool load = false;
  Establishment establishment;
  List<Establishment> establishments = [];
  List<int> selectCats = [];
  String _sort;
  String _search;
  void getList () async{
    load = true;
    establishments = await EstablishmentRepository().index('id','',[]);
    load = false;
    notifyListeners();
  }
  void search () async{
    load = true;
    establishments = await EstablishmentRepository().index(_sort,_search,selectCats);
    load = false;
    notifyListeners();
  }
  void show (int id) async{
    load = true;
    // notifyListeners();
    establishment = await EstablishmentRepository().show(id);

    load = false;
    notifyListeners();
  }

  set sort(String s){
      _sort = s;
      notifyListeners();
  }
  String get sort => _sort;

  set searchText(String s){
      _search = s;
      notifyListeners();
  }
  String get searchText => _search;
}