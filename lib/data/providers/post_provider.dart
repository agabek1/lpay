import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:lpay/data/models/establishment.dart';
import 'package:lpay/data/models/instagram_post.dart';
import 'package:lpay/data/models/post.dart';
import 'package:lpay/data/models/user.dart';
import 'package:lpay/data/repository/post_repository.dart';
import 'package:lpay/data/service/vk_service.dart';
import 'package:lpay/prefs.dart';
import 'package:http/http.dart' as http;
enum PostStatus {
  InitPage,
  Loading,
  Success,
  Error
}


class PostProvider extends ChangeNotifier {
  PostStatus _status = PostStatus.InitPage;
  PostStatus get status => _status;

  set status(PostStatus value) {
    _status = value;
    WidgetsBinding.instance.addPostFrameCallback((_){
      notifyListeners();
    });
  }

  Establishment _establishment;
  bool _vkChange = false;
  bool _instagramChange = false;
  bool _facebookChange = false;
  bool _twitterChange = false;

  int postId;
  File _image;
  String imageUrl;
  User _user;
  String _body;

  List<Post> _histories;
  List<Post> get histories => _histories;

  set histories(List<Post> value) {
    _histories = value;
  }

  Establishment get establishment => _establishment;

  bool get vkChange => _vkChange;

  bool get instagramChange => _instagramChange;

  bool get facebookChange => _facebookChange;

  bool get twitterChange => _twitterChange;

  File get image => _image;

  User get user => _user;

  String get body => _body;

  set instagramChange(bool value) {
    _instagramChange = value;
  }

  set facebookChange(bool value) {
    _facebookChange = value;
  }

  set twitterChange(bool value) {
    _twitterChange = value;
  }

  set image(File value) {
    _image = value;
  }

  set user(User value) {
    _user = value;
  }

  set body(String value) {
    _body = value;
  }

  set establishment(Establishment e) {
    _establishment = e;
  }

  set vkChange(bool value) {
    _vkChange = value;
  }


  // ignore: avoid_void_async
  void add()  async {
    final dynamic res = await PostRepository().add(accessToken: user.accessToken,body:body,image: image,establishmentId: establishment.id);
    if(res != null){
      imageUrl = res['image'].toString();
      postId = int.parse(res['id'].toString());
    }
    _status = PostStatus.Success;
    notifyListeners();
  }
  // ignore: avoid_void_async
  void getHistories()  async {
     PostRepository().getHistories(user).then((value) {

    }) as List<Post>;

  }

  Future<void> vkAdd() async{
    _status = PostStatus.Loading;
    notifyListeners();

    String vkPostId =  await VkService(imagePath: _image.path ,vkAccessToken: user.vkAccessToken,hashtag: establishment.hashtag).addPost();
    await PostRepository().update(accessToken: user.accessToken,postId: postId,vkPostId: vkPostId);
    _status = PostStatus.Success;

    notifyListeners();
  }
  Future<void> instagramUrlAdd({String instagramUrl,int postId}) async{
    _status = PostStatus.Loading;
    notifyListeners();
    final String accessToken = PreferenceUtils.getString('accessToken');
    await PostRepository().update(accessToken: accessToken,postId: postId,instagramUrl: instagramUrl);
    _status = PostStatus.Success;

    notifyListeners();
  }
  Future<void> checkInstagram() async{
    final String accessToken = PreferenceUtils.getString('accessToken');
    List<InstagramPost> urls = await PostRepository().lastInstagramPosts();

    urls.forEach((element) async {
      String page = await PostRepository().getInstagramPage(element.url);
      if  (page != null){
       var position =  page.indexOf('edge_media_preview_like":{"count":');
        if (position > 0){
          String preCount = page.substring(position,position + 70);
          var count = preCount.replaceAll('edge_media_preview_like":{"count":','').replaceAll(',"edges":[]},"edge_media_to_sponso', '');
          await PostRepository().update(accessToken: accessToken,postId: element.postId,instagramLikes: count);
        }
      }
    });
  }
}


