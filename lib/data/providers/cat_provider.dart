import 'package:flutter/cupertino.dart';
import 'package:lpay/data/models/cat.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/repository/cat_repository.dart';
import 'package:lpay/data/repository/city_repository.dart';

class CatProvider extends ChangeNotifier{
  bool load = false;
  List<Cat> cats = [];

  // CatProvider();
  void getCats () async{
    load = true;
    cats = await CatRepository().getCats();
    load = false;
    notifyListeners();
  }
}