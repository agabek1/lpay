import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/models/user.dart';
import 'package:lpay/prefs.dart';
import 'package:lpay/data/repository/city_repository.dart';
import 'package:lpay/data/repository/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum Status {
  Uninitialized, //open page
  Loader, //loadibg
  Error, //error
  Login, //success Login
  Logout, //success Login
  Auth, //success Login
  AuthError, //success Login
  Register, //Success register
  RegisterVerify, //Success register

  PasswordResetConfirmation,
  PasswordResetUpdate,
  PasswordResetUpdateSuccess,
}

class UserProvider extends ChangeNotifier {
  Status _status = Status.Uninitialized;

  Status get status => _status;
  String errorMessage;
  User user;

  set status(Status status){
    _status = status;
  }

  Future<void> register(User registerUser) async {
    _status = Status.Loader;
    try {
      await UserRepository().register(registerUser);
      user = registerUser;
      _status = Status.Register;
    } on UserNotFoundException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    } on ServerException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    } on TimeoutException catch (_) {
      _status = Status.Error;
      errorMessage = 'Time Out';
    } on FormatException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    }
    notifyListeners();
  }

  Future<void> registerVerify(String code) async {
    _status = Status.Loader;
    try {
      User _user = await UserRepository().registerValid(user.phone, code);
      _status = Status.RegisterVerify;
      print('user ... ${_user.name}');
      PreferenceUtils.setString("accessToken", _user.accessToken);
      PreferenceUtils.setInt("user_id", _user.id);

      user = _user;
    } on ErrorException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    }
    notifyListeners();
  }

  Future<void> login(String phone, String password) async {
    _status = Status.Loader;
    try {
      User _user = await UserRepository().login(phone, password);
      _status = Status.Login;
      user = _user;
      PreferenceUtils.setString("accessToken", user.accessToken);
      PreferenceUtils.setInt("user_id", user.id);
    } on UserNotFoundException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    } on ServerException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    } on TimeoutException catch (_) {
      _status = Status.Error;
      errorMessage = 'Time Out';
    } on FormatException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    }
    notifyListeners();
  }

  Future<void> logout() async {
    _status = Status.Loader;
    PreferenceUtils.clear();

    _status = Status.Logout;

    notifyListeners();
  }

  Future<void> profile(String accessToken) async {
    _status = Status.Loader;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
    try {
      _status = Status.Login;
      User _user;
      _user = await UserRepository().profile(accessToken);
      PreferenceUtils.setInt("user_id", _user.id);
      user = _user;
      notifyListeners();
    } on AuthException catch (_) {
      _status = Status.AuthError;
      errorMessage = 'Введите данные';
    } on UserNotFoundException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    } on ServerException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    } on TimeoutException catch (_) {
      _status = Status.Error;
      errorMessage = 'Time Out';
    } on FormatException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    }
    print(errorMessage);
    notifyListeners();
  }

  Future<void> editCity(City city) async {
    _status = Status.Loader;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
    _status = Status.Login;
    User _user;
    String accessToken = PreferenceUtils.getString('accessToken');
    user = await UserRepository().editCity(city.id, accessToken);
    notifyListeners();
  }

  Future<void> edit(Map<String, dynamic> params) async {
    _status = Status.Loader;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
    _status = Status.Login;
    String accessToken = PreferenceUtils.getString('accessToken');
    user = await UserRepository().edit(body: params, accessToken: accessToken);
    notifyListeners();
  }

  Future<void> passwordReset(String phone) async {
    _status = Status.Loader;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
    final String accessToken = PreferenceUtils.getString('accessToken');
    try {
      await UserRepository().passwordReset(phone: phone, accessToken: accessToken);
      _status = Status.PasswordResetConfirmation;
    } on UserNotFoundException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    } on ServerException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    } on TimeoutException catch (_) {
      _status = Status.Error;
      errorMessage = 'Time Out';
    } on FormatException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    }

    notifyListeners();
  }

  Future<void> passwordResetConfirmation({String phone, String code}) async {
    _status = Status.Loader;
    print('loader');
    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
    final String accessToken = PreferenceUtils.getString('accessToken');
    try {
      await UserRepository().passwordResetConfirmation(phone: phone,code: code, accessToken: accessToken);

      _status = Status.PasswordResetUpdate;
      print('set success');
    } on UserNotFoundException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    } on ServerException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    } on TimeoutException catch (_) {
      _status = Status.Error;
      errorMessage = 'Time Out';
    } on FormatException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    }
    print(_status);
    notifyListeners();
  }

  Future<void> passwordResetUpdate({String phone, String code, String password}) async {
    _status = Status.Loader;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
    final String accessToken = PreferenceUtils.getString('accessToken');
    try {
      await UserRepository().passwordResetUpdate(phone: phone,code: code,password: password, accessToken: accessToken);
      _status = Status.PasswordResetUpdateSuccess;
    } on UserNotFoundException catch (_) {
      _status = Status.Error;
      errorMessage = _.message;
    } on ServerException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    } on TimeoutException catch (_) {
      _status = Status.Error;
      errorMessage = 'Time Out';
    } on FormatException catch (_) {
      _status = Status.Error;
      errorMessage = 'Проблема с сервером';
    }

    notifyListeners();
  }
}
