import 'package:flutter/cupertino.dart';
import 'package:lpay/data/models/city.dart';
import 'package:lpay/data/repository/city_repository.dart';

class CityProvider extends ChangeNotifier{
  bool load = false;
  List<City> cities = [];

  // CityProvider();
  Future<List<City>> getCities () async{
    load = true;
    cities = await CityRepository().getCities();
    load = false;
    notifyListeners();

    return cities;

  }
}